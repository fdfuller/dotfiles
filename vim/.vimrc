"
" VIM Configuration 
"

" set shell to bash
set shell=/bin/sh
set encoding=utf-8

" Turn off compatible mode
set nocompatible
filetype off

set tabstop=4
set softtabstop=4
set shiftwidth=4    " number of spaces to use for autoindenting
set expandtab       " Soft tabs (spaces instead of tab characters)
set colorcolumn=85

set autoindent      " set autoindenting
set copyindent      " copy indenting on copy/paste

" Faster window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

set number          " Show line numbers
set relativenumber
set numberwidth=2
set ruler

set showmatch       " highlight matching brackets
set mat=2           " How many tenths of a second to blink

set hlsearch    " highlight search items
set incsearch   " search as you type
set gdefault
set smartcase   " if mixed case, use case when searching

set showmode
set showcmd             " show commands as they're typed
set laststatus=2
set cursorline

" No sound on errors
set visualbell
set t_vb=

" Lots of history
set history=10000
set undolevels=10000
set undofile

" Disable swap/backups
set nobackup
set noswapfile

set ffs=unix,dos,mac "Default file types
syntax enable

"set t_Co=256
set background=dark

" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors

colorscheme tender
highlight Comment cterm=italic

" Enable filetype plugin
filetype on
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" Set 7 lines to the curors - when moving vertical..

set wildmenu "Turn on WiLd menu
set cmdheight=2 "The commandbar height
set magic "Set magic on, for regular expressions
